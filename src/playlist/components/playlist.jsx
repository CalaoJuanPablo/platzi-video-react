import React from "react";
import Media from "./media";

import "./playlist.css";

export default function Playlist(props){
  const { playlist, handleOpenModal } = props;
  return(
    <div className="Playlist">
      {
        playlist.map((item) => {
          return (
            <Media
            key={ item.id }
            { ...item }
            openModal={handleOpenModal} />
          )
        })
      }
    </div>
  )
}