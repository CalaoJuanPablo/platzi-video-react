import React from 'react';
import PropTypes from 'prop-types';

import './media.css';

export default class Media extends React.Component{

  handleClick = event => {
    this.props.openModal(this.props);
  }

  render(){
    const { title, cover, author } = this.props;
    return(
      <div className="Media" onClick={ this.handleClick }>
        <div className="Media-cover">
          <img className="Media-image" 
            src={ cover }
            alt={ title }
            width={260}
            height={160}
          />
          <h3 className="Media-title">{ title }</h3>
          <p className="Media-author">{ author }</p>
        </div>
      </div>
    )
  }
}

Media.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string.isRequired,
  author: PropTypes.string
}