import React, { Component } from 'react';
import Search from '../components/search';

export default class SearchContainer extends Component{
  handleSubmit = event => {
    event.preventDefault();
    console.log("Enviar")
  }

  setInputRef = el => {
    this.input = el;
  }

  render(){
    return(
      <Search
        handleSubmit={this.handleSubmit}
        setRef={this.setInputRef}/>
    )
  }
}
