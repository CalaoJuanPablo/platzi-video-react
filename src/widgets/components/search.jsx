import React from 'react';
import './search.css';

export default function Search(props){
  return(
    <form
      className="Search"
      onSubmit={props.handleSubmit}>
      <input
        ref={props.setRef}
        type="text"
        placeholder="Busca tu video favorito"
        className="Search-input"/>
    </form>
  )
}
