import React from 'react';
import './timer.css';

export default function Timer(props){
  const { currentTime, duration } = props
  return(
    <div className="Timer">
      <p>
        <span> {currentTime} / {duration}</span>
      </p>
    </div>
  )
}