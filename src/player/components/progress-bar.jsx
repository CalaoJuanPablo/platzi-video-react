import React from 'react';
import './progress-bar.css'

export default function ProgressBar(props){
  const { 
    duration, 
    value, 
    handleProgressChange } = props
  return(
    <div className="ProgressBar">
      <input 
        type="range"
        min={0}
        max={duration}
        value={value}
        onChange={handleProgressChange}/>
    </div>
  )
}