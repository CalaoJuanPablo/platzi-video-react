import React from 'react';
import FullScreenIcon from '../../icons/components/fullscreen';
import './full-screen.css';

export default function FullScreen(props){
  const { handleFullScreenClick } = props;
  return(
    <div 
      className="FullScreen"
      onClick={handleFullScreenClick}>
      <FullScreenIcon 
        size={25}
        color="white"/>
    </div>
  )
}