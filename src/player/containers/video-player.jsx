import React, { Component } from 'react';
import VideoPlayerLayut from '../components/video-player-layout';
import Video from '../components/video';
import Title from '../components/title';
import PlayPause from '../components/play-pause';
import Timer from '../components/timer';
import Controls from '../components/video-player-controls';
import FormatedTime from '../../widgets/components/utilities';
import ProgressBar from '../components/progress-bar';
import Spinner from '../components/spinner';
import Volume from '../components/volume';
import FullScreen from '../components/full-screen';

export default class Player extends Component {
  state = {
    pause: true,
    duration: 0,
    currentTime:0,
    loading: false
  }

  togglePlay = event => {
    this.setState(prevState => ({
      pause: !prevState.pause
    }))
  }

  componentDidMount(){
    this.setState({
      pause: !this.props.autoplay
    })
  }

  handleLoadedMetadata = event => {
    this.video = event.target;
    this.setState({
      duration: this.video.duration
    })
  }

  handleTimeUpdate = event => {
    this.setState({
      currentTime: this.video.currentTime
    })
  }

  handleProgressChange = event => {
    this.video.currentTime = event.target.value;
  }

  handleSeeking = event => {
    this.setState({
      loading: true
    })
  }

  handleSeeked = event => {
    this.setState({
      loading:false
    })
  }

  handleVolumeChange = event => {
    this.video.volume = event.target.value;
  }

  handleMute = event => {
    this.video.volume = 0
  }

  handleFullScreenClick= event => {
    if (!document.isFullScreen){
      this.player.webkitRequestFullScreen();
    }else{
      document.exitFullscreen();
    }
  }

  setRef = element => {
    this.player = element
  }

  render(){
    const { title, src } = this.props;
    return(
      <VideoPlayerLayut setRef={this.setRef}>
        <Title 
          title={title}/>
        <Controls>
          <PlayPause 
            pause={this.state.pause}
            handleClick={this.togglePlay}/>
          <Timer 
            duration={FormatedTime(this.state.duration)}
            currentTime={FormatedTime(this.state.currentTime)}/>
          <ProgressBar 
            duration={this.state.duration}
            value={this.state.currentTime}
            handleProgressChange={this.handleProgressChange}/>
          <Volume 
            handleVolumeChange={this.handleVolumeChange}
            handleMute={this.handleMute}/>
          <FullScreen 
            handleFullScreenClick={this.handleFullScreenClick}/>
        </Controls>
        <Spinner 
          handleSeeking={this.handleSeeking}
          handleSeeked={this.handleSeeked}
          active={this.state.loading}/>
        <Video 
          autoplay={this.props.autoplay}
          pause={this.state.pause}
          src={src}
          handleLoadedMetadata={this.handleLoadedMetadata}
          handleTimeUpdate={this.handleTimeUpdate}/>
      </VideoPlayerLayut>
    )
  }
}