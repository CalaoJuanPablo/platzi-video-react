import React, { Component } from 'react';
import Home from './pages/containers/home';
import data from "./api.json";

import './App.css';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Home data={data} />
      </React.Fragment>
    );
  }
}

export default App;
