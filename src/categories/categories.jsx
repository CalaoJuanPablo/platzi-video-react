import React from "react";
import Category from "./category";
import Search from '../widgets/containers/search';
import "./categories.css";

export default function Categories(props){
    const { categories, handleOpenModal } = props;
    return(
      <div className="Categories">
        <Search />
        {
          categories.map((category) => {
            return (
              <Category
                key={ category.id }
                { ...category }
                handleOpenModal={handleOpenModal}/>
            )
          })
        }
      </div>
    )
}
